﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace KC_MERGE_IC
{
    /// <summary>
    /// Interaction logic for DP_Content.xaml
    /// </summary>
    public partial class DP_Content : UserControl, INotifyPropertyChanged
    {
        
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, e);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public DP_Content()
        {
            InitializeComponent();
        }
        //public String CommentBox
        //{
        //    get{return TB_Comment.ToString();}
        //    set { TB_Comment.Text = value; }
        //}
        //public static readonly DependencyProperty BaseItemCodeProperty = DependencyProperty.Register("BaseItemCode", typeof(string), typeof(DP_Content));
        //public String BaseItemCode
        //{
        //    get { return TB_BaseItemCode.Value; }
        //    set { TB_BaseItemCode.Value = value; }
        //}
    }
     
}
