﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace KC_MERGE_IC
{
    /// <summary>
    /// Interaction logic for LabelValue.xaml
    /// </summary>
    public partial class LabelValue : StackPanel   
    {
        public LabelValue()
        {
            InitializeComponent();
        }
        public String Label
        {
            get { return TB_Label.Text; }
            set { TB_Label.Text = value; }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(LabelValue));
        public String Value
        {
            get
            {
                return (String)GetValue(ValueProperty);
                //return TB_Value.Text;
            }
            set
            {
                SetValue(ValueProperty, value);
                //TB_Value.Text = value;
            }
        }
        public bool TextBoxIsReadOnly
        {
            get { return TB_Value.IsReadOnly; }
            set { TB_Value.IsReadOnly = value; }
        }
        public bool TextBoxIsEnabled
        {
            get { return TB_Value.IsEnabled; }
            set { TB_Value.IsEnabled = value; }
        }
        public TextBox TB_ValueObject
        {
            get { return TB_Value; }
        }
    }

}
