﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace KC_MERGE_IC
{
    public class DP_Content_Converter_New : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           // && value.GetType() == typeof(int)
            //targetType == typeof(int) &&
            return ( (int)value >= 0) ?  Visibility.Collapsed :Visibility.Visible;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Visibility.Collapsed;
        }
    }
}
