﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;


namespace KC_MERGE_IC
{
    public class MessageBoxSizeConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double currentWindowSize = (double)value;
            double newSize = (double)value*.9;
           // return newSize;
            if (currentWindowSize < 800)
            {
                return 600;
            }
            //else if(currentWindowSize <800)
            //{
            //    return 600;
            //}
            else
            {
                return newSize;
            }
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
