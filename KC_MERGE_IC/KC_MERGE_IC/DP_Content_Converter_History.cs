﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace KC_MERGE_IC
{
    public class DP_Content_Converter_History : IValueConverter
    {
        // && value.GetType() == typeof(int)
        //targetType == typeof(int) && 
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((int)value >= 0) ? Visibility.Visible: Visibility.Collapsed ;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Visibility.Visible;
        }
    }
}
