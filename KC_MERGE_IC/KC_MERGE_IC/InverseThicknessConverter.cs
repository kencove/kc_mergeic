﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace KC_MERGE_IC
{
    public class InverseThicknessConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (targetType == typeof(Thickness) && value.GetType() == typeof(double)) ? new Thickness(10, -(double)value, 10, 10) : new Thickness(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new Thickness(0);
        }
    }
}
