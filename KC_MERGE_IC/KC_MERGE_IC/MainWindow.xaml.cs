﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Data;
using System.Net;
using System.Security;
//using System.Data.Objects;


namespace KC_MERGE_IC
{
    public partial class MainWindow : Window
    {
        public IM_ItemCodeMerge NewItemCodeMerge { get; set; }
        public KencoveCOREEntities Entities { get; set; }
        public String UserName { get; set; }
        public bool Admin { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            EnableOnlyComment();
            DP_NewContent.TB_BaseItemCode.TB_Value.CharacterCasing = CharacterCasing.Upper;
            DP_NewContent.TB_MergingItemCode.TB_Value.CharacterCasing = CharacterCasing.Upper;
            DP_HistoryContent.TB_BaseItemCode.TB_Value.CharacterCasing = CharacterCasing.Upper;
            DP_HistoryContent.TB_MergingItemCode.TB_Value.CharacterCasing = CharacterCasing.Upper;

            DP_NewContent.TB_BaseItemCode.TB_Value.TextChanged += TB_Value_TextChanged;
            DP_NewContent.TB_MergingItemCode.TB_Value.TextChanged += TB_Value_TextChanged;
            DP_HistoryContent.TB_Comment.LostFocus += TB_Comment_TextChanged;
            UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new[] { '\\' })[1];
            Entities = new KencoveCOREEntities();

            if (Entities.UserPermissions.Count(user => user.User == UserName && user.Admin) > 0)
                Admin = true;
            else if (Entities.UserPermissions.Count(user => user.User == UserName) > 0)
                Admin = false;
            else
            {
                MessageBox.Show("You do not have permission to open this program.");
                Close();
            }

            RefreshDataContext();
        }

        private void TB_Comment_TextChanged(object sender, RoutedEventArgs e)
        {
            Entities.SaveChanges();
        }


        private void RefreshDataContext()
        {
            LB_History.ItemsSource = Entities.IM_ItemCodeMerge.ToList();
            NewItemCodeMerge = new IM_ItemCodeMerge();
            DP_NewContent.DataContext = NewItemCodeMerge;
        }

        private void TB_Value_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox value = sender as TextBox;
            if (value == null) return;

            IM_ItemCodeMerge_ItemList itemObject = (
                from item in Entities.IM_ItemCodeMerge_ItemList
                where item.ItemCode == value.Text
                select item).FirstOrDefault();

            //if (itemObject == null) return;

            LabelValue parent = (value.Parent as LabelValue);
            if (parent == null) return;

            if (parent.TB_Label.Text == "Base")
            {
                NewItemCodeMerge.IM_BaseItemCode = itemObject;
                DP_NewContent.L_BaseItemCode.Content = itemObject == null ? String.Empty : itemObject.ItemCodeDesc;
            }
            else if (parent.TB_Label.Text == "To Merge")
            {
                NewItemCodeMerge.IM_MergingItemCode = itemObject;
                DP_NewContent.L_MergingItemCode.Content = itemObject == null ? String.Empty : itemObject.ItemCodeDesc;
            }

            B_Commit.IsEnabled = ValidateNewItemCodeMerge();
        }

        private bool ValidateNewItemCodeMerge()
        {
            return NewItemCodeMerge.IM_BaseItemCode != null
                && NewItemCodeMerge.IM_BaseItemCode.ItemCodeDesc != "Item Already merged, Description Missing"
                && NewItemCodeMerge.IM_MergingItemCode != null
                && NewItemCodeMerge.IM_MergingItemCode.ItemCodeDesc != "Item Already merged, Description Missing"
                && NewItemCodeMerge.IM_BaseItemCode != NewItemCodeMerge.IM_MergingItemCode;
        }

        private void B_Commit_Click(object sender, RoutedEventArgs e)
        {
            SaveCommit();
        }

        public void SaveCommit()
        {
            try
            {

                NewItemCodeMerge.UserName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                NewItemCodeMerge.Created = DateTime.Now;
                Entities.IM_ItemCodeMerge.Add(NewItemCodeMerge);
                Entities.SaveChanges();
                if (Admin == true)
                {
                    Entities.IM_ItemCodeMerge_Email(NewItemCodeMerge.ID);
                    Approve.IsEnabled = false;
                }
                else
                {
                    Entities.IM_ItemCodeMerge_SupervisorEmail(NewItemCodeMerge.ID);
                }

                RefreshDataContext();
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (LB_History.SelectedIndex >= 0)
                LB_History.SelectedIndex = -1;
        }
        private void EnableOnlyComment()
        {
            DP_HistoryContent.TB_Approved.IsEnabled = false;
            DP_HistoryContent.TB_ApprovedBy.IsEnabled = false;
            DP_HistoryContent.TB_BaseItemCode.IsEnabled = false;
            DP_HistoryContent.TB_Comment.IsEnabled = true;
            DP_HistoryContent.TB_Completed.IsEnabled = false;
            DP_HistoryContent.TB_MergingItemCode.IsEnabled = false;
            DP_HistoryContent.TB_Message.IsEnabled = false;
            
        }



        private void B_Delete_Click(object sender, RoutedEventArgs e)
        {
            DeleteObject();
        }
        private void Approve_Click(object sender, RoutedEventArgs e)
        {
            
            if (Admin == true)
            {
                Entities.IM_ItemCodeMerge_Email(NewItemCodeMerge.ID);
                Approve.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("You do not have access to this button.");
            }
        }
        public void DeleteObject()
        {
            IM_ItemCodeMerge SelItem = new IM_ItemCodeMerge();
            SelItem = (IM_ItemCodeMerge)LB_History.SelectedItem;
            if (SelItem != null)
                Entities.IM_ItemCodeMerge.Remove(SelItem);
            Entities.SaveChanges();
            RefreshDataContext();
        }
    }
}
